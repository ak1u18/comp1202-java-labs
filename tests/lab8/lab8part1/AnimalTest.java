import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Constructor;
import java.util.ArrayList;

class AnimalTest {

	class OutputCapturer {
		private PrintStream origOut;

		private ByteArrayOutputStream outputStream;

		public void start()
		{
			this.origOut = System.out;
			this.outputStream = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(this.outputStream);
			System.setOut(ps);
		}

		public String getOutput() {
			System.out.flush();
			return this.outputStream.toString().replaceAll("\\r\\n", "\n").replaceAll("\\r", "\n");
		}
		public void stop() {
			System.setOut(this.origOut);
		}
	}

	@Test
	@DisplayName("Test the eat method")
	void eatTest() {
		OutputCapturer outputHarness;

		outputHarness = new OutputCapturer();
		outputHarness.start();

		Wolf wolf1 = new Wolf(2, "Tim");
		Meat meat1 = new Meat("Venison");
		
		int no_eat = 5;

		wolf1.eat(meat1, 5);
		
		outputHarness.stop();
		int no_noise = outputHarness.getOutput().split("\n").length;
		
		assertEquals(no_eat, no_noise, "Testing that a new line is printed with an eating message or noise");
		
	}
	
	@Test
	@DisplayName("Test that there are two eat methods")
	void eatMethodTest() {
		Wolf wolf1 = new Wolf(2, "Tim");
		Method[] methods = wolf1.getClass().getSuperclass().getSuperclass().getDeclaredMethods();
		
		ArrayList<Method> eatMethods = new ArrayList<Method> ();
		
		for(int i = 0; i<methods.length; i++) {
			String methodName = methods[i].getName();
			if(methodName.equals("eat")) {
				Parameter[] params = methods[i].getParameters();
				ArrayList<String> eatParams = new ArrayList<String> ();
				
				for(int ii=0; ii<params.length; ii++) {
					String p = params[ii].toString();
					if(p.equals("Food arg0") || p.equals("java.lang.Integer arg1")) {
						eatParams.add(p);
					}
				}
				
				if(eatParams.size()==2|| eatParams.size()==1) {
					eatMethods.add(methods[i]);
				}
			}
		}
		
		assertEquals(2, eatMethods.size(), "Testing that there are two eat methods in the Animal class");
		
	}
	
	@Test
	@DisplayName("Test Parrot age methods")
	void parrotTest() {
		Integer age = 5;
		Parrot polly = new Parrot(age);
		Constructor[] pars = polly.getClass().getConstructors();
		assertEquals(2, pars.length);
		assertEquals(age, polly.getAge(), "Testing polly's age is correct");
		
	}

	@Test
	@DisplayName("Test Wolf age methods")
	void AnimalTest() {
		Wolf wolf1 = new Wolf();
		Integer age = 5;
		Wolf wolf2 = new Wolf(age, "Fuzzy");
		
		Constructor[] pars = wolf1.getClass().getConstructors();
		assertEquals(2, pars.length);
		
		assertEquals(0, wolf1.getAge().intValue(), "Testing that the new born wolf's age is 0");
		assertEquals(age, wolf2.getAge(), "Testing that the new born wolf's age is correct");
		
	}
}
