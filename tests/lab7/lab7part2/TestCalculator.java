public class TestCalculator {

    public boolean testParser(Calculator testCalculator){
        boolean test = true;
        if(testCalculator.x("12+5").equals(new Double(17))){
            System.out.println("[PASS] Basic parsing succeeds to add.");
        } else {
            System.out.println("[FAIL] Basic parsing fails to add.");
            test = false;
        }
        if(testCalculator.x("12x5").equals(new Double(60))){
            System.out.println("[PASS] Basic parsing succeeds to multiply.");
        } else {
            System.out.println("[FAIL] Basic parsing fails to multiply.");
            test = false;
        }
        if(testCalculator.x("12[3") == null){
            System.out.println("[PASS] Parser returns null");
        } else {
            System.out.println("[FAIL] Parser does not return null for operators which are not supported.");
            test = false;
        }
        return test;
    }

    public boolean testAdd(Calculator testCalculator){
        boolean test = true;
        System.out.println("== Adding ==");
        if(testCalculator.x("5 + 2").equals(new Double(7))) {
            System.out.println("[PASS] Calculator adds correctly");
        } else {
            System.out.println("[FAIL] Calculator adds incorrectly");
            test = false;
        }

        System.out.println("== Adding ==");
        if(testCalculator.x("-1 + -2").equals(new Double(-3))) {
            System.out.println("[PASS] Calculator adds with negative numbers correctly");
        } else {
            System.out.println("[FAIL] Calculator adds with negative numbers incorrectly");
            test = false;
        }
        return test;
    }
    
    public boolean testMultiplication(Calculator testCalculator){
        boolean test = true;
        System.out.println("== Multiplying ==");
        if(testCalculator.x("5 * 3").equals(new Double(15))) {
            System.out.println("[PASS] Calculator multiplies correctly");
        } else {
            System.out.println("[FAIL] Calculator multiplies incorrectly");
            test = false;
        }
        System.out.println("== Multiplying ==");
        if(testCalculator.x("-5 * -3").equals(new Double(15))) {
            System.out.println("[PASS] Calculator multiplies negative numbers correctly");
        } else {
            System.out.println("[FAIL] Calculator multiplies negative numbers incorrectly");
            test = false;
        }
        return test;
    }
}
